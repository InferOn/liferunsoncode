var path = require('path'),
	rootPath = path.normalize(__dirname + '/..'),
	env = process.env.NODE_ENV || 'development';

var config = {
	development: {
		root: rootPath,
		app: {
			name: 'life-runs-on-code'
		},
		port: 3306,
		db: {
			database: "tmysql",
			user: "root",
			password: "",
			options: {
				host: 'localhost',
				dialect: 'mysql',
				pool: {
					max: 100,
					min: 0,
					idle: 10000
				}
			}
		}
	}
}
module.exports = config[env];