﻿var handlebars = require('express-handlebars');
var path = require('path');
module.exports = (app, config) => {
	app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
	app.set('view engine', 'handlebars');
	app.set('views', path.join(config.root, 'views'));
}