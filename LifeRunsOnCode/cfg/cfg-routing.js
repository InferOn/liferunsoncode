﻿module.exports = (app, config) => {
    app.use('/', require(config.root + '/controllers/index'));
    app.use('/api', require(config.root + '/controllers/api'));
}