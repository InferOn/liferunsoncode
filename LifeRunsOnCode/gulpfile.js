/// <binding />
var gulp = require('gulp')
var ts = require("gulp-typescript")
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps');
var minify = require('gulp-js-minify');
var less = require('gulp-less');
var path = require('path');
var LessAutoprefix = require('less-plugin-autoprefix');
var merge2 = require('merge2');
var autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] });
var tsProject = ts.createProject('tsconfig.json');
var livereload = require('gulp-livereload');

var vars = {
	src_base_path: "./src-client/",
	vendor_base_path: "./src-client/vendor/"
}

gulp.task('move_to_vendor_js', () => {
	gulp.src([
		"./node_modules/jquery/dist/jquery.min.js",
		"./node_modules/bluebird/browser/bluebird.min.js",
		"./node_modules/angular/angular.min.js",
		"./node_modules/dexie/dist/dexie.min.js",
		//"./node_modules/ng-dexie/build/ng-dexie.min.js",
		"./node_modules/ng-dexie/build/ngDexie.min.js",
		"./node_modules/angular-aria/angular-aria.min.js",
		"./node_modules/angular-animate/angular-animate.min.js",
		"./node_modules/angular-material/angular-material.min.js",
		"./node_modules/@uirouter/angularjs/release/angular-ui-router.min.js",
		"./node_modules/moment/min/moment.min.js",
		"./node_modules/humanize-duration/humanize-duration.js",
		"./node_modules/angular-timer/dist/angular-timer.min.js",
		"./node_modules/phaser/build/pixi.min.js",
		"./node_modules/phaser/build/phaser.min.js",

	])
		.pipe(gulp.dest(vars.src_base_path + "vendor/js/"))
})
gulp.task('move_to_vendor_css', () => {
	gulp.src([
		"./node_modules/angular-material/angular-material.min.css",
	])
		.pipe(gulp.dest(vars.src_base_path + "vendor/css/"))
})
gulp.task('move_to_vendor', ['move_to_vendor_css', 'move_to_vendor_js'])

gulp.task('build-vendor-js', () => {
	gulp.src([
		vars.vendor_base_path + "js/jquery.min.js",
		vars.vendor_base_path + "js/angular.min.js",
		vars.vendor_base_path + "js/angular-animate.min.js",
		vars.vendor_base_path + "js/angular-ui-router.min.js",
		vars.vendor_base_path + "js/angular-aria.min.js",
		vars.vendor_base_path + "js/angular-material.min.js",
		vars.vendor_base_path + "js/dexie.min.js",
		vars.vendor_base_path + "js/moment.min.js",
		vars.vendor_base_path + "js/humanize-duration.js",
		vars.vendor_base_path + "js/angular-timer.min.js",
	])
		.pipe(concat("vendor.min.js"))
		.pipe(gulp.dest("public/javascripts/"))
});
gulp.task('build-vendor-css', () => {
	gulp.src([
		vars.src_base_path + "**/*.css"
	])
		.pipe(concat("vendor.min.css"))
		.pipe(gulp.dest("public/stylesheets/"))
});
gulp.task('build-vendor', ['build-vendor-js', 'build-vendor-css'])

gulp.task('less', function () {
	return gulp.src(vars.src_base_path + 'less/app.less')
		.pipe(less({
			plugins: [autoprefix]
		}))
		.pipe(gulp.dest('public/stylesheets/'))
		.pipe(livereload());
});

gulp.task('watch', () => {
	livereload.listen();
	gulp.watch(vars.src_base_path + 'less/**/*.less', ["less"]);
	gulp.watch(vars.src_base_path + 'ts/**/*.ts', ["move_js_to_public"]);
})

gulp.task('move_js_to_public', () => {
	return gulp.src(vars.src_base_path + 'ts/**/*.js')
		.pipe(gulp.dest('public/javascripts/'))
		.pipe(livereload());
});
