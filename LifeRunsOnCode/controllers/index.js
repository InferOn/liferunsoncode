﻿var express = require('express');
var router = express.Router();
/* GET home page. */
router.get('/', function (req, res) {
	res.set({ 'content-type': 'text/html; charset=utf-8' })
	res.render('index', { title: 'Life runs on code...' });
});
module.exports = router;