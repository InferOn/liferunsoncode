﻿var express = require('express');
var db = require('../db');
var toonavatar = require('../node_modules/cartoon-avatar');
var Identity = require('../node_modules/fake-identity');
var router = express.Router();
const MALE_IMAGE_COUNT = 129;
const FEMALE_IMAGE_COUNT = 114;

router.get('/', function (req, res) {
	res.status(200).json({
	});
});

router.get('/character', function (req, res) {
	var identity = Identity.generate();
	let opt = identity.sex == 'male'
		? { "gender": identity.sex, "id": Math.floor(Math.random() * MALE_IMAGE_COUNT) + 1 }
		: { "gender": identity.sex, "id": Math.floor(Math.random() * FEMALE_IMAGE_COUNT) + 1 }
	var url = toonavatar.generate_avatar(opt);
	url = url.replace("https://raw.githubusercontent.com/Ashwinvalento/cartoon-avatar/master/lib/images/", "");
	let themes = ['grey', 'indigo', 'lime', 'blue-grey', 'amber',];
	let theme = themes[Math.floor(Math.random() * themes.length)];
	console.log("theme:" + theme);
	res.status(200).json({
		sex: identity.sex,
		firstName: identity.firstName,
		lastName: identity.lastName,
		dateOfBirth: identity.dateOfBirth,
		detail: {
			exp: 0,
			avatar: url,
			Theme: theme,
			emailAddress: identity.emailAddress,
			phoneNumber: identity.phoneNumber,
			street: identity.street,
			city: identity.city,
			state: identity.state,
			zipCode: identity.zipCode,
			company: identity.company,
			department: identity.department,
		}
	});
});
module.exports = router;