﻿"use strict";

namespace LifeRunsOnCode.Providers {
    export class ThemeProvider {
        private setTheme = (theme: string, accent: string, warn: string, back: string) => {
            this.$mdThemingProvider.theme(theme)
                .primaryPalette(theme)
                .accentPalette(accent)
                .warnPalette(warn)
                .backgroundPalette(back);
            this.$mdThemingProvider.enableBrowserColor({
                theme: theme,
                palette: 'accent',
                hue: '800'
            });
        }
        static $inject = ["$mdThemingProvider"];
        constructor(private $mdThemingProvider: angular.material.IThemingProvider) {
					this.setTheme('grey', 'amber', 'indigo', 'grey');
            this.setTheme('indigo', 'pink', 'blue', 'indigo');
						this.setTheme('lime', 'orange', 'lime', 'pink');
						this.setTheme('blue-grey', 'blue-grey', 'orange', 'pink');
						this.setTheme('amber', 'pink', 'orange', 'amber');
            $mdThemingProvider.alwaysWatchTheme(true);
        }
    }
}
export = LifeRunsOnCode.Providers.ThemeProvider;