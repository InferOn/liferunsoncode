﻿"use strict";

import ICharacter = require("../models/character");
import ICharacterDetail = require("../models/characterDetail");
//import Dexie from "../../../node_modules/dexie/dist/dexie";

//export namespace Dexie {
//	export interface Table<T, number> { }
//	export interface Dexie { }
//}


declare var Dexie: any;
namespace LifeRunsOnCode.Providers {
	export class LocalStorageProvider {
		character: ICharacter;
		characterDetail: ICharacterDetail;
		db: any;
		constructor() {
			this.db = new Dexie("LifeRunsOnCode");
			this.db.version(1).stores({
				character: "++id, FirstName, LastName, DoB, createdAt, updatedAt, Detail.id",
			});
		}
	}
}
export = LifeRunsOnCode.Providers.LocalStorageProvider;