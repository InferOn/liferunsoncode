﻿"use strict";

namespace LifeRunsOnCode.Providers {
		export class StateRouterProvider {
			constructor($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) {
				$stateProvider.state("dashboard", {
					name: "dashboard",
					url: "/dashboard",
					views: {
						"": {
							templateUrl: "./templates/dashboard.html",
						},
					},
				});
				$urlRouterProvider.when("/", "/dashboard");
				$urlRouterProvider.otherwise("/dashboard");
			}
		}
}
export = LifeRunsOnCode.Providers.StateRouterProvider;