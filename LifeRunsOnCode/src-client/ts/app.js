define(["require", "exports", "controllers/AppController", "directives/AppLoading", "services/BackgroundWorker", "services/CharacterServiceProvider", "providers/stateRouteProvider", "providers/local-storage-provider", "providers/themeProvider"], function (require, exports, AppController, AppLoading, BackgroundWorker, CharacterServiceProvider, StateRouterProvider, LocalStorageService, ThemeProvider) {
    "use strict";
    var app = angular.module("app", ["ui.router", "ngAnimate", "ngMaterial", "timer"])
        .controller("appController", AppController)
        .directive("mAppLoading", AppLoading)
        .service("backgroundWorker", BackgroundWorker)
        .service("ThemeProvider", ThemeProvider)
        .service("LocalStorageService", LocalStorageService)
        .service("CharacterServiceProvider", CharacterServiceProvider)
        .config(["$stateProvider", "$urlRouterProvider", "$mdThemingProvider", "$interpolateProvider",
        function ($stateProvider, $urlRouterProvider, $mdThemingProvider, $interpolateProvider) {
            $interpolateProvider.startSymbol("{[{");
            $interpolateProvider.endSymbol("}]}");
            new StateRouterProvider($stateProvider, $urlRouterProvider);
            new ThemeProvider($mdThemingProvider);
        }])
        .run(function (backgroundWorker) { });
    return app;
});
//# sourceMappingURL=app.js.map