﻿"use strict";

import AppController = require('controllers/AppController');
import AppLoading = require('directives/AppLoading');
import BackgroundWorker = require('services/BackgroundWorker');
import CharacterServiceProvider = require('services/CharacterServiceProvider');
import StateRouterProvider = require('providers/stateRouteProvider');
import LocalStorageService = require("providers/local-storage-provider");
import ThemeProvider = require('providers/themeProvider');
import Character = require('models/character');

declare var angular: any;
var app = angular.module("app", ["ui.router", "ngAnimate", "ngMaterial", "timer"])
	.controller("appController", AppController)
	.directive("mAppLoading", AppLoading)
	.service("backgroundWorker", BackgroundWorker)
	.service("ThemeProvider", ThemeProvider)
	.service("LocalStorageService", LocalStorageService)
	.service("CharacterServiceProvider", CharacterServiceProvider)
	.config(["$stateProvider", "$urlRouterProvider", "$mdThemingProvider", "$interpolateProvider",
		(
			$stateProvider: angular.ui.IStateProvider,
			$urlRouterProvider: angular.ui.IUrlRouterProvider,
			$mdThemingProvider: angular.material.IThemingProvider,
			$interpolateProvider: angular.IInterpolateProvider,
		) => {
			$interpolateProvider.startSymbol("{[{");
			$interpolateProvider.endSymbol("}]}");
			new StateRouterProvider($stateProvider, $urlRouterProvider);
			new ThemeProvider($mdThemingProvider);
		}])
	.run((backgroundWorker: BackgroundWorker) => { });
export = app;