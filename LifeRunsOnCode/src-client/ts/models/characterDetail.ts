"use strict";

namespace LifeRunsOnCode.Models {
	export class CharacterDetail {
		id: number;
		exp: number;
		avatar: string;
		Theme: string;
		emailAddress: string;
		phoneNumber: string;
		street: string;
		city: string;
		state: string;
		zipCode: number;
		company: string;
		department: string;
		createdAt: Date;
		updatedAt: Date;
	}
}
export = LifeRunsOnCode.Models.CharacterDetail;