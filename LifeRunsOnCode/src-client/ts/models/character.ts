"use strict";
import CharacterDetail = require('characterDetail');
namespace LifeRunsOnCode.Models {
	export class Character {
		id: number;
		firstName: string;
		lastName: string;
		dateOfBirth: Date;
		createdAt: Date;
		updatedAt: Date;
		fk_detail_id: number;
		detail: CharacterDetail;
	}
}
export = LifeRunsOnCode.Models.Character;