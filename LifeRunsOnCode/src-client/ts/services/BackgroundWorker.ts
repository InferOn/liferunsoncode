﻿"use strict";
namespace LifeRunsOnCode {
	export namespace Services {
		export class BackgroundWorker {
			constructor() {
				if ('serviceWorker' in navigator) {
					navigator.serviceWorker.register('./javascripts/services/serviceWorker.js');
				}
			}
		}
	}
}
export = LifeRunsOnCode.Services.BackgroundWorker;