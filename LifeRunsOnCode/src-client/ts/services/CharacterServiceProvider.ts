﻿"use strict";
import Character = require('models/character');
import LocalStorageService = require("../providers/local-storage-provider");
namespace LifeRunsOnCode {
	export namespace Services {
		export class CharacterServiceProvider {
			private Character: Character;
			private Characters: Array<Character>;
			static $inject = ["$http", "LocalStorageService"];
			constructor(private $http: angular.IHttpService, private LocalStorageService: LocalStorageService) { }
			private serverCharacterCallback(): Character {
				let ___self = this;
				let result = this.$http.get("/api/character")
					.then((response) => {
						return this.LocalStorageService
							.db.character
							.add(response.data)
							.then((rr) => {
								return ___self.GetCharacter();
							})
					});
				return result as any;
			}
			GetCharacter(): Promise<Character> {
				let ___self = this;
				return this.LocalStorageService.db.character
					.toArray()
					.then((result) => {
						if (result.length > 0) {
							return result[0];
						}
						return ___self.serverCharacterCallback();
					})
					.catch((e) => { throw new Error(e) })
			}
		}
	}
}
export = LifeRunsOnCode.Services.CharacterServiceProvider;