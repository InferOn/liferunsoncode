const RUNTIME = 'runtime';
const PRECACHE = 'precache-v1';
const BASE_PATH = "../../";
const PRECACHE_URLS = [
	BASE_PATH + 'stylesheets/app.css',
	BASE_PATH + 'javascripts/require.js',
	BASE_PATH + 'javascripts/vendor.min.js',
];

self.addEventListener('install', event => {
	console.log("hit");
	(event as any).waitUntil(
		caches.open(PRECACHE)
			.then(cache => cache.addAll(PRECACHE_URLS))
			.then((self as any).skipWaiting())
	);
});

self.addEventListener('activate', event => {
	console.log('activate');
	const currentCaches = [PRECACHE, RUNTIME];
	(event as any).waitUntil(
		caches.keys().then(cacheNames => {
			console.log("get cache");
			return cacheNames.filter(cacheName => !(currentCaches as any).includes(cacheName));
		}).then(cachesToDelete => {
			return Promise.all(cachesToDelete.map(cacheToDelete => {
				console.log("delete cache");
				return caches.delete(cacheToDelete);
			}));
		}).then(() => (self as any).clients.claim())
	);
});

self.addEventListener('fetch', event => {
	console.log('fetch');
	// Skip cross-origin requests, like those for Google Analytics.
	if ((event as any).request.url.startsWith(self.location.origin)) {
		(event as any).respondWith(
			caches.match((event as any).request).then(cachedResponse => {
				if (cachedResponse) {
					console.log('cachedResponse');
					return cachedResponse;
				}
				return caches.open(RUNTIME).then(cache => {
					return fetch((event as any).request).then(response => {
						// Put a copy of the response in the runtime cache.
						return cache.put((event as any).request, response.clone()).then(() => {
							console.log('cachedResponse');
							return response;
						});
					});
				});
			})
		);
	}
});