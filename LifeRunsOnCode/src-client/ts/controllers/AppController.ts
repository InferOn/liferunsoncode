﻿"use strict";
import IAppScope = require('scopes/IAppScope');
import Character = require('models/character');
import CharacterServiceProvider = require('services/CharacterServiceProvider');
declare var angular;

namespace LifeRunsOnCode.Controllers {
	export class AppController {
		static $inject = ["$scope", "CharacterServiceProvider", "$timeout", "$mdSidenav"];
		constructor(private $scope: IAppScope,
			private characterServiceProvider: CharacterServiceProvider,
			private $timeout: angular.ITimeoutService,
			private $mdSidenav: angular.material.ISidenavService) {
			this.$scope.theme = 'grey';
			this.$scope.startingTime = null;
			this.$scope.startTime = () => {
				this.$scope.$broadcast("timer-start");
			}
			this.$scope.stopTime = () => {
				this.$scope.$broadcast("timer-stop");
			}
			this.characterServiceProvider.
				GetCharacter().then((result) => {
					this.$scope.Character = angular.copy(result);
					console.log(this.$scope.Character.detail.avatar);
					$timeout(() => {
						this.$scope.topbar_expanded = "lroc-topbar-expanded";
					}, 500).then(() => {
						$timeout(() => {
							this.$scope.sidenav_expanded = "sidenav-expanded";
						}, 500).then(() => {
							$timeout(() => {
								this.$scope.ready_fast = "lroc-visible--grow--fast";
								this.$scope.ready_fastest = "lroc-visible--grow--fastest";
								this.$scope.topbar_title_expanded = "lroc-topbar-title--expanded";
								this.$scope.topbar_time_running_expanded  = "lroc-topbar-time-running--expanded";

								this.$scope.timeRunning = new Date(this.$scope.Character.dateOfBirth.valueOf()).getTime();
							}, 500)
						})
					});
				})
		}
	}
}
export = LifeRunsOnCode.Controllers.AppController;