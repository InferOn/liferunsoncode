define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Controllers;
        (function (Controllers) {
            var AppController = (function () {
                function AppController($scope, characterServiceProvider, $timeout, $mdSidenav) {
                    var _this = this;
                    this.$scope = $scope;
                    this.characterServiceProvider = characterServiceProvider;
                    this.$timeout = $timeout;
                    this.$mdSidenav = $mdSidenav;
                    this.$scope.theme = 'grey';
                    this.$scope.startingTime = null;
                    this.$scope.startTime = function () {
                        _this.$scope.$broadcast("timer-start");
                    };
                    this.$scope.stopTime = function () {
                        _this.$scope.$broadcast("timer-stop");
                    };
                    this.characterServiceProvider.
                        GetCharacter().then(function (result) {
                        _this.$scope.Character = angular.copy(result);
                        console.log(_this.$scope.Character.detail.avatar);
                        $timeout(function () {
                            _this.$scope.topbar_expanded = "lroc-topbar-expanded";
                        }, 500).then(function () {
                            $timeout(function () {
                                _this.$scope.sidenav_expanded = "sidenav-expanded";
                            }, 500).then(function () {
                                $timeout(function () {
                                    _this.$scope.ready_fast = "lroc-visible--grow--fast";
                                    _this.$scope.ready_fastest = "lroc-visible--grow--fastest";
                                    _this.$scope.topbar_title_expanded = "lroc-topbar-title--expanded";
                                    _this.$scope.topbar_time_running_expanded = "lroc-topbar-time-running--expanded";
                                    _this.$scope.timeRunning = new Date(_this.$scope.Character.dateOfBirth.valueOf()).getTime();
                                }, 500);
                            });
                        });
                    });
                }
                return AppController;
            }());
            AppController.$inject = ["$scope", "CharacterServiceProvider", "$timeout", "$mdSidenav"];
            Controllers.AppController = AppController;
        })(Controllers = LifeRunsOnCode.Controllers || (LifeRunsOnCode.Controllers = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Controllers.AppController;
});
//# sourceMappingURL=AppController.js.map