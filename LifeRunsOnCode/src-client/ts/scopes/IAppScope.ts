﻿"use strict";

import Character = require('models/character');

interface IAppScope extends ng.IScope {
	Character: Character;
	theme: any;
	ready_fastest: string;
	ready_fast: string;
	is_locked_open: boolean;
	toolbar_is_open: boolean;
	topbar_expanded: string;
	sidenav_expanded: string;
	topbar_title_expanded: string;
	topbar_time_running_expanded : string;

	timeRunning: number;
	startTime: Function;
	stopTime: Function;
	startingTime: Date;
}
export = IAppScope;



