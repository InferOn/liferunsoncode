define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Directives;
        (function (Directives) {
            function AppLoading($animate) {
                return {
                    restrict: "C",
                    scope: {},
                    link: function (scope, element, attributes) {
                        $animate.leave(element.children().eq(1)).then(function () {
                            element.remove();
                            scope = element = attributes = null;
                        });
                    }
                };
            }
            Directives.AppLoading = AppLoading;
        })(Directives = LifeRunsOnCode.Directives || (LifeRunsOnCode.Directives = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Directives.AppLoading;
});
//# sourceMappingURL=AppLoading.js.map