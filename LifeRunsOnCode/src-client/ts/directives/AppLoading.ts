﻿"use strict";

import IAppScope = require('scopes/IAppScope');
namespace LifeRunsOnCode.Directives {
	export function AppLoading($animate: angular.animate.IAnimateService): ng.IDirective {
		return {
			restrict: "C",
			scope: {},
			link: (scope: IAppScope, element: JQuery<HTMLElement>, attributes) => {
				$animate.leave(element.children().eq(1)).then(() => {
					element.remove();
					scope = element = attributes = null;
				});
			}
		}
	}
}

export = LifeRunsOnCode.Directives.AppLoading;
