define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Models;
        (function (Models) {
            var CharacterDetail = (function () {
                function CharacterDetail() {
                }
                return CharacterDetail;
            }());
            Models.CharacterDetail = CharacterDetail;
        })(Models = LifeRunsOnCode.Models || (LifeRunsOnCode.Models = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Models.CharacterDetail;
});
