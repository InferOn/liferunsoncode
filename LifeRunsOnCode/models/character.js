define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Models;
        (function (Models) {
            var Character = (function () {
                function Character() {
                }
                return Character;
            }());
            Models.Character = Character;
        })(Models = LifeRunsOnCode.Models || (LifeRunsOnCode.Models = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Models.Character;
});
