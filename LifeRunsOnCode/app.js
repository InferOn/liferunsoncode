﻿var express = require('express'),
	cfg = require('./cfg/cfg'),
	db = require('./db'),
	seed = require('./db/seed'),
	app = express();
require('./cfg/cfg-express')(app, cfg);

function startApp() {
	
	/**
		- Seeding
	*/
	//seed.Seed(db);
}
db.sequelize.sync({ force: true })
	.then(startApp)
	.catch(function (e) {
		throw new Error(e);
	});

module.exports = app;

