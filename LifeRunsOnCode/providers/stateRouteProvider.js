define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Providers;
        (function (Providers) {
            var StateRouterProvider = (function () {
                function StateRouterProvider($stateProvider, $urlRouterProvider) {
                    $stateProvider.state("dashboard", {
                        name: "dashboard",
                        url: "/dashboard",
                        views: {
                            "": {
                                templateUrl: "./templates/dashboard.html",
                            },
                        },
                    });
                    $urlRouterProvider.when("/", "/dashboard");
                    $urlRouterProvider.otherwise("/dashboard");
                }
                return StateRouterProvider;
            }());
            Providers.StateRouterProvider = StateRouterProvider;
        })(Providers = LifeRunsOnCode.Providers || (LifeRunsOnCode.Providers = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Providers.StateRouterProvider;
});
