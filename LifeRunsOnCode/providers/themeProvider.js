define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Providers;
        (function (Providers) {
            var ThemeProvider = (function () {
                function ThemeProvider($mdThemingProvider) {
                    var _this = this;
                    this.$mdThemingProvider = $mdThemingProvider;
                    this.setTheme = function (theme, accent, warn, back) {
                        _this.$mdThemingProvider.theme(theme)
                            .primaryPalette(theme)
                            .accentPalette(accent)
                            .warnPalette(warn)
                            .backgroundPalette(back);
                        _this.$mdThemingProvider.enableBrowserColor({
                            theme: theme,
                            palette: 'accent',
                            hue: '800'
                        });
                    };
                    this.setTheme('grey', 'grey', 'grey', 'grey');
                    this.setTheme('indigo', 'pink', 'blue', 'indigo');
                    this.setTheme('lime', 'orange', 'pink', 'lime');
                    this.setTheme('blue-grey', 'pink', 'orange', 'blue-grey');
                    this.setTheme('amber', 'pink', 'orange', 'amber');
                    $mdThemingProvider.alwaysWatchTheme(true);
                }
                ThemeProvider.$inject = ["$mdThemingProvider"];
                return ThemeProvider;
            }());
            Providers.ThemeProvider = ThemeProvider;
        })(Providers = LifeRunsOnCode.Providers || (LifeRunsOnCode.Providers = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Providers.ThemeProvider;
});
