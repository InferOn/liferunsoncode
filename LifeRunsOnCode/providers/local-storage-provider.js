define(["require", "exports"], function (require, exports) {
    "use strict";
    var LifeRunsOnCode;
    (function (LifeRunsOnCode) {
        var Providers;
        (function (Providers) {
            var LocalStorageProvider = (function () {
                function LocalStorageProvider() {
                    this.db = new Dexie("LifeRunsOnCode");
                    this.db.version(1).stores({
                        character: "++id, FirstName, LastName, DoB, createdAt, updatedAt, Detail.id",
                    });
                }
                return LocalStorageProvider;
            }());
            Providers.LocalStorageProvider = LocalStorageProvider;
        })(Providers = LifeRunsOnCode.Providers || (LifeRunsOnCode.Providers = {}));
    })(LifeRunsOnCode || (LifeRunsOnCode = {}));
    return LifeRunsOnCode.Providers.LocalStorageProvider;
});
