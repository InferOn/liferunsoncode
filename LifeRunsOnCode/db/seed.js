var toonavatar = require('../node_modules/cartoon-avatar');
var Identity = require('../node_modules/fake-identity');
const MALE_IMAGE_COUNT = 129;
const FEMALE_IMAGE_COUNT = 114;

var seed = {};
seed.Seed = (db) => {
	var seedChar = (db, char, detail) => {
		char.Detail = detail;
		db.Character.create(char, { include: [{ model: db.CharacterDetail, as: 'Detail' }] })
			.then((obj, result) => { })
			.catch((e) => { throw new Error(e) });
	}
	let profiles = new Array(1);

	profiles.forEach((profile) => {
		var identity = Identity.generate();
		let opt = identity.sex == 'male'
			? { "gender": identity.sex, "id": Math.floor(Math.random() * MALE_IMAGE_COUNT) + 1 }
			: { "gender": identity.sex, "id": Math.floor(Math.random() * FEMALE_IMAGE_COUNT) + 1 }
		var url = toonavatar.generate_avatar(opt);
		let p = 0;
		//let p = {
		//	sex: identity.sex,
		//	firstName: identity.firstName,
		//	lastName: identity.lastName,
		//	dateOfBirth: identity.dateOfBirth,
		//	detail: {
		//		exp: 0,
		//		avatar: DataTypes.STRING,
		//		Theme: DataTypes.STRING,
		//		emailAddress: DataTypes.STRING,
		//		phoneNumber: DataTypes.STRING,
		//		street: DataTypes.STRING,
		//		city: DataTypes.STRING,
		//		state: DataTypes.STRING,
		//		zipCode: DataTypes.INTEGER,
		//		company: DataTypes.STRING,
		//		department: DataTypes.STRING,
		//	}
		//};
		//seedChar(db, , );
	});
	//let profiles = [
	//	{
	//		master: {
	//			FirstName: 'Profile',
	//			LastName: 'A',
	//			DoB: new Date(1980, 10, 20),
	//		},
	//		detail: { Avatar: 'avatar-03', Theme: 'amber', Exp: 0 }
	//	},
	//	{
	//		master: {
	//			FirstName: 'Profile',
	//			LastName: 'B',
	//			DoB: new Date(1972, 10, 02),
	//		},
	//		detail: { Avatar: 'avatar-05', Theme: 'lime', Exp: 0 }
	//	},
	//	{
	//		master: {
	//			FirstName: 'Profile',
	//			LastName: 'C',
	//			DoB: new Date(1946, 10, 10),
	//		},
	//		detail: { Avatar: 'avatar-07', Theme: 'amber', Exp: 0 }
	//	},
	//];

	//profiles.forEach((profile) => {
	//	seedChar(db, profile.master, profile.detail);
	//});
}
module.exports = seed;
