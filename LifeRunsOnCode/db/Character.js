module.exports = (sequelize, DataTypes) => {
	const Character = sequelize.define('Character', {
		sex: DataTypes.STRING,
		firstName: DataTypes.STRING,
		lastName: DataTypes.STRING,
		dateOfBirth: DataTypes.DATE,
		createdAt: DataTypes.DATE,
		updatedAt: DataTypes.DATE
	}, {
			classMethods: {
				associate: (models) => {
					Character.belongsTo(models.CharacterDetail, {
						foreignKey: 'fk_detail_id',
						targetKey: 'id',
						as: 'Detail'
					})
				}
			}
		});
	return Character;
}