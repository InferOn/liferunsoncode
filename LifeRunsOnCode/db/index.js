const CLASSMETHODS = 'classMethods';
const ASSOCIATE = 'associate';
var fs = require('fs'),
	path = require('path'),
	Sequelize = require('sequelize'),
	cfg = require('../cfg/cfg'),
	db = {};
var sequelize = new Sequelize(cfg.db.database, cfg.db.user, cfg.db.password, cfg.db.options);
	fs.readdirSync(__dirname).filter(function (file) {
	return (file.indexOf('.') !== 0) && (file !== 'index.js' && file !== 'seed.js');
}).forEach(function (file) {
	var model = sequelize['import'](path.join(__dirname, file));
	db[model.name] = model;
	});
Object.keys(db).forEach(function (modelName) {
	if (CLASSMETHODS in db[modelName].options) {
		if (ASSOCIATE in db[modelName].options[CLASSMETHODS]) {
			db[modelName].options.classMethods.associate(db);
		}
	}
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
