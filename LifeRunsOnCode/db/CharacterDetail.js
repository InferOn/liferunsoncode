module.exports = (sequelize, DataTypes) => {
	const CharacterDetail = sequelize.define('CharacterDetail', {
		exp: DataTypes.INTEGER,
		avatar: DataTypes.STRING,
		Theme: DataTypes.STRING,
		emailAddress: DataTypes.STRING,
		phoneNumber: DataTypes.STRING,
		street: DataTypes.STRING,
		city: DataTypes.STRING,
		state: DataTypes.STRING,
		zipCode: DataTypes.INTEGER,
		company: DataTypes.STRING,
		department: DataTypes.STRING,
		createdAt: DataTypes.DATE,
		updatedAt: DataTypes.DATE
	});
	return CharacterDetail;
}